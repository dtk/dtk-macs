// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkMacsExport>

#include <QtCore>

class DTKMACS_EXPORT dtkMacsRedirector : public QObject
{
    Q_OBJECT

public:
    enum ProcessChannel {
        StandardOutput = 1,
        StandardError = 2
    };

    Q_DECLARE_FLAGS(ProcessChannels, ProcessChannel)

public:
     dtkMacsRedirector(QObject *parent = 0, ProcessChannels channels=StandardOutput);
    ~dtkMacsRedirector(void);

    qint64 bytesAvailable(void) const;

    QByteArray read(qint64 maxlen);

signals:
    void readyRead(void);

private slots:
    void onSocketActivated(void);

private:
    class dtkMacsRedirectorPrivate *d;
};

Q_DECLARE_OPERATORS_FOR_FLAGS(dtkMacsRedirector::ProcessChannels)

//
// dtkMacsRedirector.h ends here
