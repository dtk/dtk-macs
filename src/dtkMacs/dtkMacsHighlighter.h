// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <QtCore>
#include <QtGui>
#include <QtWidgets>

#include <dtkMacsExport>

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class DTKMACS_EXPORT dtkMacsHighlightRule
{
public:
    dtkMacsHighlightRule(void);
    dtkMacsHighlightRule(QRegularExpression pattern, QString format);

public:
    QString format;

public:
    QRegularExpression pattern;
};

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class DTKMACS_EXPORT dtkMacsHighlighter : public QSyntaxHighlighter
{
    Q_OBJECT

public:
     dtkMacsHighlighter(QObject *parent = nullptr);
    ~dtkMacsHighlighter(void);

public slots:
    virtual void rehighlight(void) = 0;
    virtual void rehighlightBlock(const QTextBlock&) = 0;

protected:
    void load(const QString&);

protected:
    QStringList keys(void);
    QStringList names(const QString& key);

private:
    class dtkMacsHighlighterPrivate *d;
};

//
// dtkMacsHighlighter.h ends here
