// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#import <Foundation/Foundation.h>

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

FOUNDATION_EXPORT NSErrorDomain const LSPResponseError;

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

typedef NS_ENUM(NSUInteger, dtkLSPErrorCode) {
    dtkLSPResponseParseError = -32700,
    dtkLSPResponseInvalidRequest = -32600,
    dtkLSPResponseMethodNotFound = -32601,
    dtkLSPResponseInvalidParams = -32602,
    dtkLSPResponseInternalError = -32603,
    dtkLSPResponseServerErrorStart = -32099,
    dtkLSPResponseServerErrorEnd = -32000,
    dtkLSPResponseServerNotInitialized = -32002,
    dtkLSPResponseUnknownErrorCode = -32001,
    dtkLSPResponseRequestCancelled = -32800,
    dtkLSPResponseContentModified = -32801
};

typedef NS_ENUM(NSUInteger, dtkLSPMessageType) {
    dtkLSPMessageTypeError = 1,
    dtkLSPMessageTypeWarning = 2,
    dtkLSPMessageTypeInfo = 3,
    dtkLSPMessageTypeLog = 4
};

typedef NS_ENUM(NSUInteger, dtkLSPDocumentHighlightKind) {
    dtkLSPDocumentHighlightKindText = 1,
    dtkLSPDocumentHighlightKindRead = 2,
    dtkLSPDocumentHighlightKindWrite = 3,
};

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

@interface dtkLSPPosition : NSObject

@property (readonly) NSUInteger line;
@property (readonly) NSUInteger character;

+ (instancetype)positionForCharacterAtIndex:(NSUInteger)location inText:(NSString *)string;
+ (instancetype)positionFromDictionary:(NSDictionary *)dict;
+ (instancetype)positionWithLine:(NSUInteger)line character:(NSUInteger)character;

- (NSUInteger)convertToPositionInText:(NSString *)string;
- (NSDictionary *)params;

@end

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

@interface dtkLSPRange : NSObject

@property (readonly) dtkLSPPosition *start;
@property (readonly) dtkLSPPosition *end;

+ (instancetype)range:(NSRange)range inText:(NSString *)string;
+ (instancetype)rangeFromDictionary:(NSDictionary *)dict;

- (NSRange)convertToRangeInText:(NSString *)string;

@end

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

typedef NS_ENUM(NSUInteger, dtkLSPDiagnosticSeverity) {
    dtkLSPDiagnosticSeverityUnknown = 0,
    dtkLSPDiagnosticSeverityError = 1,
    dtkLSPDiagnosticSeverityWarning = 2,
    dtkLSPDiagnosticSeverityInformation = 3,
    dtkLSPDiagnosticSeverityHint = 4,
};

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

@interface dtkLSPDiagnostic : NSObject

@property (readonly) dtkLSPRange *range;
@property (readonly) dtkLSPDiagnosticSeverity severity;
@property (readonly) id code;
@property (readonly) NSString *source;
@property (readonly) NSString *message;
@property (readonly) id relatedInformation;

+ (NSArray<dtkLSPDiagnostic *> *)diagnosticsFromArray:(NSArray *)array;
+ (instancetype)diagnosticFromDictionary:(NSDictionary *)dict;

@end

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

@interface dtkLSPDocumentHighlight : NSObject

@property (readonly) NSRange range;
@property (readonly) dtkLSPDocumentHighlightKind kind;

- (instancetype)initWithRange:(NSRange)range kind:(dtkLSPDocumentHighlightKind)kind;

@end

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

typedef NS_ENUM(NSUInteger, dtkLSPCompletionItemKind) {
    dtkLSPCompletionItemKindText = 1,
    dtkLSPCompletionItemKindMethod = 2,
    dtkLSPCompletionItemKindFunction = 3,
    dtkLSPCompletionItemKindConstructor = 4,
    dtkLSPCompletionItemKindField = 5,
    dtkLSPCompletionItemKindVariable = 6,
    dtkLSPCompletionItemKindClass = 7,
    dtkLSPCompletionItemKindInterface = 8,
    dtkLSPCompletionItemKindModule = 9,
    dtkLSPCompletionItemKindProperty = 10,
    dtkLSPCompletionItemKindUnit = 11,
    dtkLSPCompletionItemKindValue = 12,
    dtkLSPCompletionItemKindEnum = 13,
    dtkLSPCompletionItemKindKeyword = 14,
    dtkLSPCompletionItemKindSnippet = 15,
    dtkLSPCompletionItemKindColor = 16,
    dtkLSPCompletionItemKindFile = 17,
    dtkLSPCompletionItemKindReference = 18,
    dtkLSPCompletionItemKindFolder = 19,
    dtkLSPCompletionItemKindEnumMember = 20,
    dtkLSPCompletionItemKindConstant = 21,
    dtkLSPCompletionItemKindStruct = 22,
    dtkLSPCompletionItemKindEvent = 23,
    dtkLSPCompletionItemKindOperator = 24,
    dtkLSPCompletionItemKindTypeParameter = 25,
};

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

@interface dtkLSPCompletionItem : NSObject

@property (readonly) NSString *label;
@property (readonly) dtkLSPCompletionItemKind kind;
@property (readonly) NSString *detail;
@property (readonly) NSString *documentation;
@property (readonly, getter=isDeprecated) BOOL deprecated;
@property (readonly, getter=isPreselect) BOOL preselect;
@property (readonly) NSString *sortText;
@property (readonly) NSString *filterText;
@property (readonly) NSString *insertText;
@property (readonly) NSString *insertTextFormat;
@property (readonly) id textEdit;
@property (readonly) NSArray<id> *additionalTextEdits;
@property (readonly) NSArray<NSString *> *commitCharacters;
@property (readonly) id command;
@property (readonly) id data;

- (instancetype)initWithDictionary:(NSDictionary *)dict;

@end

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

@class dtkLSPClient;

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

@protocol dtkLSPClientObserver <NSObject>

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

@optional
- (void)languageServer:(dtkLSPClient *)client logMessage:(NSString *)message type:(dtkLSPMessageType)type;
- (void)languageServer:(dtkLSPClient *)client showMessage:(NSString *)message type:(dtkLSPMessageType)type;
- (void)languageServer:(dtkLSPClient *)client showMessageRequest:(NSString *)message actions:(NSArray<NSString *> *)actions;
- (void)languageServer:(dtkLSPClient *)client telemetryEvent:(id)event;
- (void)languageServer:(dtkLSPClient *)client document:(NSURL *)url diagnostics:(NSArray<dtkLSPDiagnostic *> *)diagnostics;
@end

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

typedef NS_ENUM(NSUInteger, dtkLSPTextDocumentSyncKind) {
    dtkLSPTextDocumentSyncKindNone = 0,
    dtkLSPTextDocumentSyncKindFull = 1,
    dtkLSPTextDocumentSyncKindIncremental = 2,
};

typedef struct _dtkLSPTextDocumentSyncOptions {
    BOOL openClose;
    BOOL willSave;
    BOOL willSaveWaitUntil;
    BOOL saveOptionIncludeText;

    dtkLSPTextDocumentSyncKind change;

} dtkLSPTextDocumentSyncOptions;

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

@interface dtkLSPClient : NSObject

@property (readonly) dtkLSPTextDocumentSyncOptions textDocumentSync;
@property (readonly, getter=hasHoverProvider) BOOL hoverProvider;
@property (readonly, getter=hasCompletionProvider) BOOL completionProvider;
@property (readonly, getter=hasCompletionResolveProvider) BOOL completionResolveProvider;
@property (readonly) NSArray<NSString *> *completionTriggerCharacters;
@property (readonly, getter=hasSignatureHelpProvider) BOOL signatureHelpProvider;
@property (readonly) NSArray<NSString *> *signatureHelpProviderTriggerCharacters;
@property (readonly, getter=hasDefinitionProvider) BOOL definitionProvider;
@property (readonly) BOOL typeDefinitionProvider;
@property (readonly) BOOL implementationProvider;
@property (readonly, getter=hasReferencesProvider) BOOL referencesProvider;
@property (readonly, getter=hasDocumentHighlightProvider) BOOL documentHighlightProvider;
@property (readonly, getter=hasDocumentSymbolProvider) BOOL documentSymbolProvider;
@property (readonly) BOOL workspaceSymbolProvider;
@property (readonly) BOOL codeActionProvider;
@property (readonly, getter=hasCodeLensProvider) BOOL codeLensProvider;
@property (readonly, getter=hasCodeLensResolveProvider) BOOL codeLensResolveProvider;
@property (readonly) BOOL documentFormattingProvider;
@property (readonly, getter=hasDocumentRangeFormattingProvider) BOOL documentRangeFormattingProvider;
@property (readonly, getter=hasDocumentOnTypeFormattingProvider) BOOL documentOnTypeFormattingProvider;
@property (readonly) NSString *documentOnTypeFormattingFirstTriggerCharacter;
@property (readonly) NSArray<NSString *> *documentOnTypeFormattingMoreTriggerCharacter;
@property (readonly, getter=hasRenameProvider) BOOL renameProvider;
@property (readonly, getter=hasRenamePrepareProvider) BOOL renamePrepareProvider;
@property (readonly, getter=hasDocumentLinkProvider) BOOL documentLinkProvider;
@property (readonly) BOOL documentLinkProviderResolveProvider;
@property (readonly, getter=hasColorProvider) BOOL colorProvider;
@property (readonly) BOOL colorProviderDynamicRegistration;
@property (readonly, getter=hasFoldingRangeProvider) BOOL foldingRangeProvider;
@property (readonly) BOOL executeCommandProvider;
@property (readonly) NSArray<NSString *> *executeCommandCommands;
@property (readonly) BOOL workspace;

#pragma mark Servers

+ (instancetype)sharedBashServer;
+ (instancetype)sharedHTMLServer;

- (instancetype)initWithPath:(NSString *)path arguments:(NSArray<NSString *> *)arguments currentDirectoryPath:(NSString *)currentDirectoryPath languageID:(NSString *)languageID;

@property (readonly) NSString *languageID;

#pragma mark Observers

- (void)addObserver:(id<dtkLSPClientObserver>)observer;
- (void)removeObserver:(id<dtkLSPClientObserver>)observer;

#pragma mark General

- (void)initialWithCompletionHandler:(void (^)(NSError *error))completionHandler;
- (void)addTerminationObserver:(id)object block:(void (^)(dtkLSPClient *client))block;
- (void)removeTerminationObserver:(id)object;

- (void)shutdownWithCompletionHandler:(void (^)(NSError *error))completionHandler;
- (void)terminate;

#pragma mark Text Synchronization

- (void)documentDidOpen:(NSURL *)url content:(NSString *)text;
- (void)document:(NSURL *)url changeTextInRange:(NSRange)affectedCharRange replacementString:(NSString *)replacementString;
- (void)documentDidChange:(NSURL *)url;
- (void)documentWillSave:(NSURL *)url;
- (void)documentDidSave:(NSURL *)url;
- (void)documentDidClose:(NSURL *)url;

#pragma mark Language Features

- (void)documentCompletion:(NSURL *)url inText:(NSString *)string forCharacterAtIndex:(NSUInteger)characterIndex completionHandler:(void (^)(NSArray<dtkLSPCompletionItem *> *completionList, BOOL isIncomplete, NSError *error))completionHandler ;
- (void)documentSymbol:(NSURL *)url completionHandler:(void (^)(NSArray *symbols, NSError *error))completionHandler;
- (void)documentHighlight:(NSURL *)url inText:(NSString *)string forCharacterAtIndex:(NSUInteger)characterIndex completionHandler:(void (^)(NSArray<dtkLSPDocumentHighlight *> *, NSError *error))completionHandler;

- (void)documentHoverWithContentsOfURL:(NSURL *)url inText:(NSString *)string forCharacterAtIndex:(NSUInteger)characterIndex completionHandler:(void (^)(NSDictionary *dict, NSError *error))completionHandler;

@end

//
// dtkMacsLSPClient.h ends here
