// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkMacsHighlighter.h"

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

dtkMacsHighlightRule::dtkMacsHighlightRule(void)
{

}

dtkMacsHighlightRule::dtkMacsHighlightRule(QRegularExpression pattern, QString format)
{
    this->pattern = pattern;
    this->format = format;
}

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class dtkMacsHighlighterPrivate
{
public:
    QMap<QString, QStringList> values;
};

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

dtkMacsHighlighter::dtkMacsHighlighter(QObject *parent) : QSyntaxHighlighter(parent)
{
    d = new dtkMacsHighlighterPrivate;
}

dtkMacsHighlighter::~dtkMacsHighlighter(void)
{
    delete d;
}

void dtkMacsHighlighter::load(const QString& path)
{
    QFile file(path);

    if(!file.open(QIODevice::ReadOnly))
        return;

    QXmlStreamReader reader(&file);

    QString name;
    QStringList values;

    bool readText = false;

    while (!reader.atEnd() && !reader.hasError()) {

        auto type = reader.readNext();

        if (type == QXmlStreamReader::TokenType::StartElement) {

            if (reader.name() == "section") {

                if (!values.empty()) {
                    d->values[name] = values;
                    values.clear();
                }

                name = reader.attributes().value("name").toString();

            } else if (reader.name() == "name") {
                readText = true;
            }

        } else if (type == QXmlStreamReader::TokenType::Characters && readText) {

            values << reader.text().toString();
            readText = false;
        }
    }

    if (!values.empty())
        d->values[name] = values;
}

QStringList dtkMacsHighlighter::keys(void)
{
    return d->values.keys();
}

QStringList dtkMacsHighlighter::names(const QString& key)
{
    return d->values.value(key);
}

//
// dtkMacsHighlighter.cpp ends here
