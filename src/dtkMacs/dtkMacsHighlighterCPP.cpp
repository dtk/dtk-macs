// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkMacsHighlighterCPP.h"

#include <dtkThemes>

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class dtkMacsHighlighterCPPPrivate
{
public:
    void initalize(void);

public:
    QVector<dtkMacsHighlightRule> highlight_rules;

public:
    QRegularExpression incl_pattern;
    QRegularExpression func_pattern;
    QRegularExpression type_pattern;
    QRegularExpression cstt_pattern;
    QRegularExpression cend_pattern;

public:
    dtkMacsHighlighterCPP *q;
};

void dtkMacsHighlighterCPPPrivate::initalize(void)
{
    q->load(":dtkMacs/dtkMacsHighlighterCPP.xml");

    auto keys = q->keys();

    for (auto&& key : keys) {

        auto names = q->names(key);

        for (auto&& name : names)
            this->highlight_rules.append({QRegularExpression(QString(R"(\b%1\b)").arg(name)), key});
    }
}

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

dtkMacsHighlighterCPP::dtkMacsHighlighterCPP(QObject *parent) : dtkMacsHighlighter(parent)
{
    d = new dtkMacsHighlighterCPPPrivate; d->q = this;

    d->incl_pattern = QRegularExpression(R"(#include\s+([<"][a-zA-Z0-9*._]+[">]))");
    d->func_pattern = QRegularExpression(R"(\b([A-Za-z0-9_]+(?:\s+|::))*([A-Za-z0-9_]+)(?=\())");
    d->type_pattern = QRegularExpression(R"(\b([A-Za-z0-9_]+)\s+[A-Za-z]{1}[A-Za-z0-9_]+\s*[;=])");
    d->cstt_pattern = QRegularExpression(R"(/\*)");
    d->cend_pattern = QRegularExpression(R"(\*/)");

    d->highlight_rules.append({QRegularExpression(R"(\b(0b|0x){0,1}[\d.']+\b)"), "Number"});
    d->highlight_rules.append({QRegularExpression(R"("[^\n"]*")"), "String"});
    d->highlight_rules.append({QRegularExpression(R"(#[a-zA-Z_]+)"), "Preprocessor"});
    d->highlight_rules.append({QRegularExpression(R"(/[^\n]*)"), "Comment"});

    d->initalize();
}

dtkMacsHighlighterCPP::~dtkMacsHighlighterCPP(void)
{
    delete d;
}

void dtkMacsHighlighterCPP::rehighlight(void)
{
    QSyntaxHighlighter::rehighlight();
}

void dtkMacsHighlighterCPP::rehighlightBlock(const QTextBlock& block)
{

}

void dtkMacsHighlighterCPP::highlightBlock(const QString& text)
{
    { // includes

        auto matchIterator = d->incl_pattern.globalMatch(text);

        while (matchIterator.hasNext())
        {
            auto match = matchIterator.next();

            setFormat(
                match.capturedStart(),
                match.capturedLength(),
                dtkThemesEngine::instance()->color("@blue")
            );

            setFormat(
                match.capturedStart(1),
                match.capturedLength(1),
                dtkThemesEngine::instance()->color("@green")
            );
        }
    }

    { // functions

        auto matchIterator = d->func_pattern.globalMatch(text);

        while (matchIterator.hasNext())
        {
            auto match = matchIterator.next();

            setFormat(
                match.capturedStart(),
                match.capturedLength(),
                dtkThemesEngine::instance()->color("@yellow")
            );

            setFormat(
                match.capturedStart(2),
                match.capturedLength(2),
                dtkThemesEngine::instance()->color("@cyan")
            );
        }
    }

    { // types

        auto matchIterator = d->type_pattern.globalMatch(text);

        while (matchIterator.hasNext())
        {
            auto match = matchIterator.next();

            setFormat(
                match.capturedStart(1),
                match.capturedLength(1),
                dtkThemesEngine::instance()->color("@yellow")
            );
        }
    }

    for (auto& rule : d->highlight_rules)
    {
        auto matchIterator = rule.pattern.globalMatch(text);

        while (matchIterator.hasNext())
        {
            auto match = matchIterator.next();

            setFormat(
                match.capturedStart(),
                match.capturedLength(),
                dtkThemesEngine::instance()->color("@red")
            );
        }
    }

    setCurrentBlockState(0);

    int startIndex = 0;

    if (previousBlockState() != 1)
    {
        startIndex = text.indexOf(d->cstt_pattern);
    }

    while (startIndex >= 0)
    {
        auto match = d->cend_pattern.match(text, startIndex);

        int endIndex = match.capturedStart();
        int commentLength = 0;

        if (endIndex == -1)
        {
            setCurrentBlockState(1);
            commentLength = text.length() - startIndex;
        }
        else
        {
            commentLength = endIndex - startIndex + match.capturedLength();
        }

        setFormat(
            startIndex,
            commentLength,
            dtkThemesEngine::instance()->color("@grey")
        );

        startIndex = text.indexOf(d->cstt_pattern, startIndex + commentLength);
    }
}

//
// dtkMacsHighlighterCPP.cpp ends here
