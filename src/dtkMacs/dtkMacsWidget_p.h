// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <QtCore>
#include <QtGui>
#include <QtWidgets>

class dtkMacsHighlighter;
class dtkMacsRedirector;
class dtkMacsWidget;
class dtkMacsWidgetLineNumberArea;

class dtkMacsWidgetPrivate : public QObject
{
    Q_OBJECT

public:
    int getFirstVisibleBlock(void);

public:
    void highlightCurrentLine(QList<QTextEdit::ExtraSelection>&);
    void highlightParenthesis(QList<QTextEdit::ExtraSelection>& extraSelection);

public slots:
    void updateExtraSelection(void);
    void updateLineGeometry(void);
    void updateLineNumberAreaWidth(int);
    void updateLineNumberArea(const QRect&);

public:
      QChar charUnderCursor(int = 0) const;
    QString wordUnderCursor(void)  const;

public:
    dtkMacsHighlighter *highlighter = nullptr;
    dtkMacsRedirector *redirector = nullptr;

public:
    dtkMacsWidget *q = nullptr;
    dtkMacsWidgetLineNumberArea *area = nullptr;

public:
    QTimer timer;
};

//
// dtkMacsWidget_p.h ends here
