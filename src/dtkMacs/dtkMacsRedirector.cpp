// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#include "dtkMacsRedirector.h"

#ifdef Q_OS_WIN

#ifndef UNICODE
#define UNICODE
#define UNICODE_WAS_UNDEFINED
#endif
#include <qt_windows.h>
#include <private/qwindowspipereader_p.h>
#include <io.h>
#include <fcntl.h>

#else
#include <private/qringbuffer_p.h>
#include <unistd.h>
#include <sys/ioctl.h>
#endif

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class dtkMacsRedirectorPrivate
{
public:
    dtkMacsRedirector::ProcessChannels m_channels;
    QRingBuffer *buffer;
#ifdef Q_OS_WIN
    HANDLE hRead;
    HANDLE hWrite;
    QWindowsPipeReader *pipeReader;
#else
    int pipeEnds[2];
    QSocketNotifier *socketNotifier;

    int o_err;
    int o_out;
#endif
};

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

#ifdef Q_OS_WIN
static void createWinPipe(HANDLE &hRead, HANDLE &hWrite)
{
    QString pipeName = QString::fromLatin1("\\\\.\\pipe\\stdoutredirector-%1").arg(QUuid::createUuid().toString());
    SECURITY_ATTRIBUTES attributes = {sizeof(SECURITY_ATTRIBUTES), 0, false};
    hRead = ::CreateNamedPipe((wchar_t*)pipeName.utf16(), PIPE_ACCESS_INBOUND | FILE_FLAG_OVERLAPPED,
                                  PIPE_TYPE_BYTE | PIPE_WAIT, 1, 0, 1024 * 1024, 0, &attributes);

    SECURITY_ATTRIBUTES attributes2 = {sizeof(SECURITY_ATTRIBUTES), 0, true};
    hWrite = ::CreateFile((wchar_t*)pipeName.utf16(), GENERIC_WRITE,
                        0, &attributes2, OPEN_EXISTING, FILE_FLAG_OVERLAPPED, NULL);

    ::ConnectNamedPipe(hRead, NULL);
}
#endif

dtkMacsRedirector::dtkMacsRedirector(QObject *parent, ProcessChannels channels) : QObject(parent)
{
    d = new dtkMacsRedirectorPrivate;
    d->m_channels = channels;

    d->buffer = new QRingBuffer;

#ifdef Q_OS_WIN
    createWinPipe(d->hRead, d->hWrite);
    if (d->m_channels & StandardOutput)
        ::SetStdHandle(STD_OUTPUT_HANDLE, d->hWrite);
    if (d->m_channels & StandardError)
        ::SetStdHandle(STD_ERROR_HANDLE, d->hWrite);

    int fd = _open_osfhandle((intptr_t)d->hWrite, _O_WRONLY|_O_TEXT);
    if (d->m_channels & StandardOutput)
        _dup2(fd, 1);
    if (d->m_channels & StandardError)
        _dup2(fd, 2);
    _close(fd);

    d->pipeReader = new QWindowsPipeReader(this);
    d->pipeReader->setHandle(d->hRead);
    d->pipeReader->startAsyncRead();
    connect(d->pipeReader, SIGNAL(readyRead()), this, SIGNAL(readyRead()));
#else

    d->o_out = dup(fileno(stdout));
    d->o_err = dup(fileno(stderr));

    ::pipe(d->pipeEnds);
    if (d->m_channels & StandardOutput)
        ::dup2(d->pipeEnds[1], 1);
    if (d->m_channels & StandardError)
        ::dup2(d->pipeEnds[1], 2);
    ::close(d->pipeEnds[1]);

    d->socketNotifier = new QSocketNotifier(d->pipeEnds[0], QSocketNotifier::Read, this);

    connect(d->socketNotifier, SIGNAL(activated(int)), this, SLOT(onSocketActivated()));
#endif
}

dtkMacsRedirector::~dtkMacsRedirector(void)
{
#ifdef Q_OS_WIN
    d->pipeReader->stop();
    ::DisconnectNamedPipe(d->hRead);
//    ::CloseHandle(d->hWrite);
#else
    delete d->buffer;

    ::dup2(d->o_out, fileno(stdout));
    ::dup2(d->o_err, fileno(stderr));

    ::close(d->o_out);
    ::close(d->o_err);

#endif
    delete d;
}

qint64 dtkMacsRedirector::bytesAvailable(void) const
{
#ifdef Q_OS_WIN
    return d->pipeReader->bytesAvailable();
#else

    return d->buffer->size();
#endif
}

QByteArray dtkMacsRedirector::read(qint64 maxlen)
{
#ifdef Q_OS_WIN
    QByteArray result(int(maxlen), Qt::Uninitialized);
    qint64 readBytes = d->pipeReader->read(result.data(), result.size());
    if (readBytes <= 0)
        result.clear();
    else
        result.resize(int(readBytes));
    return result;
#else

    return d->buffer->read();
#endif
}

void dtkMacsRedirector::onSocketActivated(void)
{
#ifdef Q_OS_UNIX

    int bytesQueued;

    if (::ioctl(d->pipeEnds[0], FIONREAD, &bytesQueued) == -1)
        return;

    if (bytesQueued <=0)
        return;

    char *writePtr = d->buffer->reserve(bytesQueued);

    int bytesRead = ::read(d->pipeEnds[0], writePtr, bytesQueued);

    if (bytesRead < bytesQueued)
        d->buffer->chop(bytesQueued - bytesRead);

    emit readyRead();
#endif
}

// ///////////////////////////////////////////////////////////////////
// Credits
// ///////////////////////////////////////////////////////////////////

/*********************************************************************
** Copyright (c) 2013 Debao Zhang <hello@debao.me>
** All right reserved.
**
** Permission is hereby granted, free of charge, to any person
** obtaining a copy of this software and associated documentation
** files (the "Software"), to deal in the Software without
** restriction, including without limitation the rights to use, copy,
** modify, merge, publish, distribute, sublicense, and/or sell copies
** of the Software, and to permit persons to whom the Software is
** furnished to do so, subject to the following conditions:
**
** The above copyright notice and this permission notice shall be
** included in all copies or substantial portions of the Software.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
** EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
** MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
** NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
** BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
** ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
** CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
** SOFTWARE.
**
*********************************************************************/

//
// dtkMacsRedirector.cpp ends here
