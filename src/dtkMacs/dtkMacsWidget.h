// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <QtCore>
#include <QtWidgets>

#include <dtkMacsExport>

class DTKMACS_EXPORT dtkMacsWidget : public QTextEdit
{
    Q_OBJECT

public:
     dtkMacsWidget(QWidget *parent = nullptr);
    ~dtkMacsWidget(void);

protected:
    void paintEvent(QPaintEvent *event);
    void resizeEvent(QResizeEvent *event);

private:
    class dtkMacsWidgetPrivate *d;

private:
    friend class dtkMacsWidgetPrivate;
    friend class dtkMacsWidgetLineNumberArea;
};

//
// dtkMacsWidget.h ends here
