// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <QtCore>
#include <QtGui>
#include <QtWidgets>

#include <dtkMacsExport>

#include "dtkMacsHighlighter.h"

class DTKMACS_EXPORT dtkMacsHighlighterCPP : public dtkMacsHighlighter
{
public:
     dtkMacsHighlighterCPP(QObject *parent = 0);
    ~dtkMacsHighlighterCPP(void);

public slots:
    void rehighlight(void) override;
    void rehighlightBlock(const QTextBlock&) override;

protected:
    void highlightBlock(const QString& text) override;

private:
    class dtkMacsHighlighterCPPPrivate *d;

private:
    friend class dtkMacsHighlighterCPPPrivate;
};

//
// dtkMacsHighlighterCPP.h ends here
