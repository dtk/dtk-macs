// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkMacsHighlighter.h"
#include "dtkMacsHighlighterCPP.h"
#include "dtkMacsRedirector.h"
#include "dtkMacsWidget.h"
#include "dtkMacsWidget_p.h"

#include <QtWidgets>

#include <dtkFonts>
#include <dtkThemes>

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class dtkMacsWidgetLineNumberArea : public QWidget
{
    Q_OBJECT

public:
    dtkMacsWidgetLineNumberArea(dtkMacsWidget* parent = nullptr);
    dtkMacsWidgetLineNumberArea(const dtkMacsWidgetLineNumberArea&) = delete;
    dtkMacsWidgetLineNumberArea& operator=(const dtkMacsWidgetLineNumberArea&) = delete;

public:
    QSize sizeHint(void) const override;

protected:
    void paintEvent(QPaintEvent *event) override;

private:
    dtkMacsWidget *parent = nullptr;
};

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

dtkMacsWidgetLineNumberArea::dtkMacsWidgetLineNumberArea(dtkMacsWidget *parent) : QWidget(parent)
{
    this->parent = parent;
}

QSize dtkMacsWidgetLineNumberArea::sizeHint(void) const
{
    if (!this->parent)
        return QWidget::sizeHint();


    int digits = 1;
    int max = qMax(1, this->parent->document()->blockCount());

    while (max >= 10) {
        max /= 10;
        ++digits;
    }

#if 0
    int space = 13 + this->parent->fontMetrics().horizontalAdvance(QLatin1Char('9')) * digits;
#else
    int space = 13 + 5 * digits;
#endif

    return { space, 0 };
}

void dtkMacsWidgetLineNumberArea::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);

    auto blockNumber = this->parent->d->getFirstVisibleBlock();
    auto block       = this->parent->document()->findBlockByNumber(blockNumber);
    auto top         = (int) this->parent->document()->documentLayout()->blockBoundingRect(block).translated(0, -this->parent->verticalScrollBar()->value()).top();
    auto bottom      = top + (int) this->parent->document()->documentLayout()->blockBoundingRect(block).height();

    auto currentLine = dtkThemesEngine::instance()->color("@base4").lighter();
    auto otherLines  = dtkThemesEngine::instance()->color("@base3").lighter();

    painter.setFont(this->parent->font());

    while (block.isValid() && top <= event->rect().bottom()) {

        if (block.isVisible() && bottom >= event->rect().top()) {

            QString number = QString::number(blockNumber + 1);

            auto isCurrentLine = this->parent->textCursor().blockNumber() == blockNumber;

            painter.setPen(isCurrentLine ? currentLine : otherLines);
            painter.drawText(-5, top, sizeHint().width(), this->parent->fontMetrics().height(), Qt::AlignRight, number);
        }

        block = block.next();
        top = bottom;
        bottom = top + (int) this->parent->document()->documentLayout()->blockBoundingRect(block).height();
        ++blockNumber;
    }
}

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

static QVector<QPair<QString, QString>> parentheses = {
    {"(", ")"},
    {"{", "}"},
    {"[", "]"},
    {"\"", "\""}
};

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

dtkMacsWidget::dtkMacsWidget(QWidget *parent) : QTextEdit(parent)
{
    d = new dtkMacsWidgetPrivate;
    d->q = this;
    d->area = new dtkMacsWidgetLineNumberArea(this);
    d->highlighter = new dtkMacsHighlighterCPP(this);
    d->highlighter->setDocument(this->document());

    // ///////////////////////////////////////////////////////////////

    dtkFontSourceCodePro::instance()->initFontSourceCodePro();

    QFont font = dtkFontSourceCodePro::instance()->font(13);

    this->setFont(font);

// ///////////////////////////////////////////////////////////////////

    connect(document(), &QTextDocument::blockCountChanged, d, &dtkMacsWidgetPrivate::updateLineNumberAreaWidth);
    connect(verticalScrollBar(), &QScrollBar::valueChanged, [this](int) { d->area->update(); });
    connect(this, &QTextEdit::cursorPositionChanged, d, &dtkMacsWidgetPrivate::updateExtraSelection);
}

dtkMacsWidget::~dtkMacsWidget(void)
{
    delete d;
}

void dtkMacsWidget::paintEvent(QPaintEvent *event)
{
    d->updateLineNumberArea(event->rect());

    QTextEdit::paintEvent(event);
}

void dtkMacsWidget::resizeEvent(QResizeEvent *event)
{
    QTextEdit::resizeEvent(event);

    d->updateLineGeometry();
}

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

QChar dtkMacsWidgetPrivate::charUnderCursor(int offset) const
{
    auto block = q->textCursor().blockNumber();
    auto index = q->textCursor().positionInBlock();
    auto text = q->document()->findBlockByNumber(block).text();

    index += offset;

    if (index < 0 || index >= text.size())
        return {};

    return text[index];
}

QString dtkMacsWidgetPrivate::wordUnderCursor(void) const
{
    auto tc = q->textCursor(); tc.select(QTextCursor::WordUnderCursor);

    return tc.selectedText();
}

int dtkMacsWidgetPrivate::getFirstVisibleBlock(void)
{
    QTextCursor curs = QTextCursor(q->document());
    curs.movePosition(QTextCursor::Start);

    for(int i = 0; i < q->document()->blockCount(); ++i) {

        QTextBlock block = curs.block();

        QRect r1 = q->viewport()->geometry();
        QRect r2 = q->document()
            ->documentLayout()
            ->blockBoundingRect(block)
            .translated(
                q->viewport()->geometry().x(),
                q->viewport()->geometry().y() - q->verticalScrollBar()->sliderPosition()
            ).toRect();

        if (r1.intersects(r2))
            return i;

        curs.movePosition(QTextCursor::NextBlock);
    }

    return 0;
}

void dtkMacsWidgetPrivate::highlightCurrentLine(QList<QTextEdit::ExtraSelection>& extraSelection)
{
    if (!q->isReadOnly())
    {
        QTextEdit::ExtraSelection selection{};

        selection.format.setBackground(dtkThemesEngine::instance()->color("@bg").darker());
        selection.format.setForeground(QBrush());
        selection.format.setProperty(QTextFormat::FullWidthSelection, true);
        selection.cursor = q->textCursor();
        selection.cursor.clearSelection();

        extraSelection.append(selection);
    }
}

void dtkMacsWidgetPrivate::updateExtraSelection(void)
{
    QList<QTextEdit::ExtraSelection> extra;

    this->highlightCurrentLine(extra);
    this->highlightParenthesis(extra);

    q->setExtraSelections(extra);
}

void dtkMacsWidgetPrivate::updateLineGeometry(void)
{
    QRect cr = q->contentsRect();

    this->area->setGeometry(QRect(cr.left(), cr.top(), this->area->sizeHint().width(), cr.height()));
}

void dtkMacsWidgetPrivate::updateLineNumberAreaWidth(int)
{
    q->setViewportMargins(this->area->sizeHint().width(), 0, 0, 0);
}

void dtkMacsWidgetPrivate::updateLineNumberArea(const QRect& rect)
{
    this->area->update(0, rect.y(), this->area->sizeHint().width(), rect.height());

    this->updateLineGeometry();

    if (rect.contains(q->viewport()->rect())) {
        this->updateLineNumberAreaWidth(0);
    }
}

void dtkMacsWidgetPrivate::highlightParenthesis(QList<QTextEdit::ExtraSelection>& extraSelection)
{
    auto currentSymbol = this->charUnderCursor();
    auto prevSymbol = this->charUnderCursor(-1);

    for (auto& pair : parentheses)
    {
        int direction;

        QChar counterSymbol;
        QChar activeSymbol;
        auto position = q->textCursor().position();

        if (pair.first == currentSymbol)
        {
            direction = 1;
            counterSymbol = pair.second[0];
            activeSymbol = currentSymbol;
        }
        else if (pair.second == prevSymbol)
        {
            direction = -1;
            counterSymbol = pair.first[0];
            activeSymbol = prevSymbol;
            position--;
        }
        else
        {
            continue;
        }

        auto counter = 1;

        while (counter != 0 &&
               position > 0 &&
               position < (q->document()->characterCount() - 1))
        {
            position += direction;

            auto character = q->document()->characterAt(position);

            if (character == activeSymbol)
                ++counter;
            else if (character == counterSymbol)
                --counter;
        }

        QTextCharFormat format;
        format.setForeground(dtkThemesEngine::instance()->color("@red"));

        if (counter == 0) {

            QTextEdit::ExtraSelection selection{};

            auto directionEnum =
                 direction < 0 ?
                 QTextCursor::MoveOperation::Left
                 :
                 QTextCursor::MoveOperation::Right;

            selection.format = format;
            selection.cursor = q->textCursor();
            selection.cursor.clearSelection();
            selection.cursor.movePosition(
                directionEnum,
                QTextCursor::MoveMode::MoveAnchor,
                std::abs(q->textCursor().position() - position)
            );

            selection.cursor.movePosition(
                QTextCursor::MoveOperation::Right,
                QTextCursor::MoveMode::KeepAnchor,
                1
            );

            extraSelection.append(selection);

            selection.cursor = q->textCursor();
            selection.cursor.clearSelection();
            selection.cursor.movePosition(
                directionEnum,
                QTextCursor::MoveMode::KeepAnchor,
                1
            );

            extraSelection.append(selection);
        }

        break;
    }
}

// ///////////////////////////////////////////////////////////////////

#include "dtkMacsWidget.moc"

//
// dtkMacsWidget.cpp ends here
