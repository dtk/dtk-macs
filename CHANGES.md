# ChangeLog

## version 2.0.3 - 2019-07-04
 - add missing export in dtkMacsRedirector
## version 2.0.2 - 2019-07-04
 - fix build on windows
## version 2.0.1 - 2019-07-03
 - bugfix restore pipes on destruction
## version 2.0.0 - 2019-07-01
 - initial release with redirection of console errors/outputs
