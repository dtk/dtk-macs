// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include <QtCore>
#include <QtWidgets>

#include <dtkFonts>
#include <dtkThemes>
#include <dtkWidgets>
#include <dtkMacs>

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

void setup(dtkApplication *);

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

int main(int argc, char **argv)
{
    dtk::widgets::initialize();
    dtkThemesEngine::instance()->apply(); // TODO

    dtkApplication *application = dtkApplication::create(argc, argv);
    application->setApplicationName("dtkMacsED");
    application->setApplicationVersion("2.0.0");
    application->setOrganizationName("inria");
    application->setOrganizationDomain("fr");
    application->initialize();
    application->setup(setup);

    return application->exec();
}

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

void setup(dtkApplication *application)
{
    QWidget *central = new QWidget(application->window());

    dtkMacsWidget *widget = new dtkMacsWidget(central);

    QHBoxLayout *layout = new QHBoxLayout;
    layout->setContentsMargins(0, 0, 0, 0);
    layout->setSpacing(0);
    layout->addWidget(application->window()->menubar());
    layout->addWidget(widget);

    central->setLayout(layout);

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

    dtkWidgetsMenu *menu_1 = new dtkWidgetsMenu(fa::filetext, "Language server");

    dtkWidgetsMenuItem *menuitem_11 = menu_1->addItem(fa::circleo, "Item 11");
    dtkWidgetsMenuItem *menuitem_12 = menu_1->addItem(fa::circleo, "Item 12");
    menu_1->addSeparator();
    dtkWidgetsMenuItem *menuitem_13 = menu_1->addItem(fa::circleo, "Item 13");


// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

    application->window()->menubar()->addMenu(menu_1);
    application->window()->populate();
    application->window()->setCentralWidget(central);
    application->window()->menubar()->touch();
}

//
// main.cpp ends here
